package com.example.caendill.shopapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by user on 10.12.2017.
 */

public class DbHandler extends SQLiteOpenHelper{

    private static final int DB_VERSION = 1;

    public final static String DATABASE_NAME = "products.db";
    public final static String TABLE_PRODUCTS = "PRODUCT";
    public final static String COLUMN_ID = "ID";
    public final static String COLUMN_NAME = "NAME";
    public final static String COLUMN_PRICE = "PRICE";
    public final static String COLUMN_BARCODE = "BARCODE";
    public final static String COLUMN_IMAGEID = "IMAGEID";
    public final static String COLUMN_NAMEPL = "NAMEPL";

    public static final String TABLE_CREATE = "CREATE TABLE " + TABLE_PRODUCTS+ " ("+
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_NAME +" TEXT, " +
            COLUMN_PRICE +" INTEGER, " +
            COLUMN_BARCODE +" BIGINT, " +
            COLUMN_IMAGEID +" INTEGER, " +
            COLUMN_NAMEPL +" TEXT" +
            ");";

    public DbHandler(Context context){
        super(context,DATABASE_NAME,null,DB_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTS);
        onCreate(db);
    }

    public boolean addProduct(Product p){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME, p.getName());
        contentValues.put(COLUMN_PRICE, p.getPrice());
        contentValues.put(COLUMN_BARCODE, p.getBarcode());
        contentValues.put(COLUMN_IMAGEID, p.getImageID());
        contentValues.put(COLUMN_NAMEPL, p.getNamePL());

        long result = db.insert(TABLE_PRODUCTS, null , contentValues);
        db.close();
        if(result == -1) {
            return false;
        }else {
            return true;
        }
    }


    public int updateProduct(Product p){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NAME, p.getName());
        contentValues.put(COLUMN_PRICE, p.getPrice());
        contentValues.put(COLUMN_BARCODE, p.getBarcode());
        contentValues.put(COLUMN_IMAGEID, p.getImageID());
        contentValues.put(COLUMN_NAMEPL, p.getNamePL());

        return  db.update(TABLE_PRODUCTS,contentValues,COLUMN_ID + " = ?",new String[] {String.valueOf(p.getId())});
    }

    public  void delProduct(Product p){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PRODUCTS, COLUMN_ID+" = ?", new String[] {String.valueOf(p.getId())});
        db.close();
    }

    public Cursor getList(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data  = db.rawQuery("SELECT * FROM "+TABLE_PRODUCTS,null);
        data.moveToFirst();

        return data;
    }

    public Product getByBarcode(long Barcode){
        SQLiteDatabase db = this.getReadableDatabase();
        List<Product> products = new ArrayList();
        Product product = new Product();
        Cursor data  = db.rawQuery("SELECT * FROM "+TABLE_PRODUCTS,null);
        data.moveToFirst();
        while(data.moveToNext() ){
            if ((data.getLong(3)) == Barcode){
                product = new Product(
                    (Integer.parseInt(data.getString(0))),
                    (data.getString(1)),
                    (data.getString(5)),
                   (Integer.parseInt(data.getString(2))),
                    (Long.parseLong(data.getString(3))),
                    (Integer.parseInt(data.getString(4)))
            );
                break;
            }
        }

        return  product;
    }
}
