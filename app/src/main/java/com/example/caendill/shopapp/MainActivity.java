package com.example.caendill.shopapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.view.Menu;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.Intent;

public class MainActivity extends AppCompatActivity {

    DbHandler dbHandler;
    ListView productsListView;
    ArrayList <Product> products = new ArrayList();
    Button scanButton;
    TextView priceSumText;
    ProductsListAdapter productsListAdapter;
    Product product;
    Integer sum = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        scanButton = (Button) findViewById(R.id.scanButton);
        priceSumText = (TextView) findViewById(R.id.priceSum);
        productsListView  = (ListView) findViewById(R.id.productsListView);

        priceSumText.setText(getResources().getString(R.string.sumPriceText)+": "+Integer.toString(sum));
        dbHandler = new DbHandler(this);

        AddData(new Product("butter", "maslo", 7,10L, R.drawable.butter));
        AddData(new Product("cheese", "ser", 3, 111L, R.drawable.cheese));
        AddData(new Product("chocolate", "czekolada", 6, 5555L, R.drawable.chocolate));
        AddData(new Product("juice", "sok",2, 5901677000629L, R.drawable.juice));
        AddData(new Product("milk", "mleko", 3, 99L, R.drawable.milk));
        AddData(new Product("bread","chleb",2,1L,R.drawable.bread));
        AddData(new Product("bun","bulka",1,22L,R.drawable.bun));
        AddData(new Product("egg","jajko",2,33L,R.drawable.egg));
        AddData(new Product("potato","ziemniak",5,44L,R.drawable.potato));
        AddData(new Product("salt","sol",10,55L,R.drawable.salt));
        AddData(new Product("square bread", "kwadratowy chleb" , 15 , 66L,R.drawable.square_bread));


//        products.add(product);
//        while(cursor.moveToNext()){
//                product = new Product(
//                    (Integer.parseInt(cursor.getString(0))),
//                    (cursor.getString(1)),
//                    (cursor.getString(5)),
//                    (Integer.parseInt(cursor.getString(2))),
//                    (Integer.parseInt(cursor.getString(3))),
//                    (Integer.parseInt(cursor.getString(4)))
//            );
//            products.add(product);
//        }

        productsListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        productsListAdapter = new ProductsListAdapter(this, products);
        productsListView.setAdapter(productsListAdapter);
        productsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Product product = (Product) parent.getItemAtPosition(position);
                if (product.isSelected()) {
                    product.setSelected(false);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        view.setBackgroundColor(getResources().getColor(R.color.itemUnselected, null));
                    } else
                        view.setBackgroundColor(getResources().getColor(R.color.itemUnselected));
                } else {
                    product.setSelected(true);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        view.setBackgroundColor(getResources().getColor(R.color.itemSelected, null));
                    } else
                        view.setBackgroundColor(getResources().getColor(R.color.itemSelected));
                }

            }



       });



        scanButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                Intent intent = new Intent(getBaseContext(),ScanActivity.class);
                startActivityForResult(intent,123);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        long barcode = 9L;
        if (resultCode == RESULT_OK && requestCode == 123){
            if (data.hasExtra("scanResult")) {
                barcode = Long.parseLong(data.getExtras().getString("scanResult"));
                product = dbHandler.getByBarcode(barcode);
                askAboutProduct(product);

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)  {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.menuDel:
                for (int i = products.size()-1 ; i >= 0 ; i--){
                    if(products.get(i).isSelected()){
                        sum -= products.get(i).getPrice();
                    products.remove(i);
                    priceSumText.setText(Integer.toString(sum));
                    productsListAdapter.notifyDataSetChanged();
                    ProductsListAdapter productsListAdapter = new ProductsListAdapter(MainActivity.this, products);
                    productsListView.setAdapter(productsListAdapter);
                }}
                break;

        }

        return true;
    }

    private void askAboutProduct(final Product p){
        if (p.getName() != null){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String name;
        if (Locale.getDefault().getDisplayLanguage().equals("polski"))
            name = p.getNamePL();
        else
            name = p.getName();
        builder.setTitle(getResources().getString(R.string.addTitleAlert1)+" "+name+" "+getResources().getString(R.string.addTitleAlert2));

        builder.setPositiveButton(R.string.possitiveAlertButon, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                products.add(product);
                sum += product.getPrice();
                priceSumText.setText( getResources().getString(R.string.sumPriceText)+": "+Integer.toString(sum));
                productsListAdapter.notifyDataSetChanged();
                ProductsListAdapter productsListAdapter = new ProductsListAdapter(MainActivity.this, products);
                productsListView.setAdapter(productsListAdapter);

            }
        });
        builder.setNegativeButton(R.string.negativeAlertButon, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
            ImageView image = new ImageView(this);
            image.setImageResource(p.getImageID());
        builder.setView(image);
        AlertDialog alert = builder.create();
        alert.show();

    }}



    public void AddData(Product p){
        boolean insertData = dbHandler.addProduct(p);

        if(insertData==true){
  //          Toast.makeText(MainActivity.this,"Entered",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(MainActivity.this,"Wrong",Toast.LENGTH_LONG).show();
        }
    }

}
