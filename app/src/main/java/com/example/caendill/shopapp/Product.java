package com.example.caendill.shopapp;

/**
 * Created by Caendill on 2018-01-13.
 */

public class Product {

    private int id;
    private String name;
    private int price;
    private long barcode;
    private int imageID;
    private String namePL = null;
    private boolean isSelected = false;

    public Product() {
    }

    public Product(String name, int price, long barcode, int imageID) {
        this.name = name;
        this.price = price;
        this.barcode = barcode;
        this.imageID = imageID;
    }

    public Product(int id, String name,String namePL, int price, long barcode, int imageID ) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.barcode = barcode;
        this.imageID = imageID;
        this.namePL = namePL;
    }

    public Product( String name,String namePL, int price, long barcode, int imageID ) {
        this.name = name;
        this.price = price;
        this.barcode = barcode;
        this.imageID = imageID;
        this.namePL = namePL;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageID() {
        return imageID;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public long getBarcode() {
        return barcode;
    }

    public void setBarcode(long barcode) {
        this.barcode = barcode;
    }

    public String getNamePL() {
        return namePL;
    }

    public void setNamePL(String namePL) {
        this.namePL = namePL;
    }

}
