package com.example.caendill.shopapp;

/**
 * Created by Caendill on 2018-01-13.
 */

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

public class ProductsListAdapter extends BaseAdapter {

    private List<Product> products;
    private final LayoutInflater inflater;
    Context ctx;

    public ProductsListAdapter(Context context, List<Product> list) {
        ctx = context;
        products = (list);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position)  {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View getView(int position, View view, ViewGroup parent) {

        View rowProduct = inflater.inflate(R.layout.product_row, parent ,false);



        TextView productName = (TextView) rowProduct.findViewById(R.id.productName);
        TextView productPrice = (TextView) rowProduct.findViewById(R.id.productPrice);
        ImageView productImage = (ImageView) rowProduct.findViewById(R.id.productImage);

        Product product = (Product) getItem(position);
        if (Locale.getDefault().getDisplayLanguage().equals("polski")){
            productName.setText(product.getNamePL());
            productPrice.setText(ctx.getResources().getString(R.string.priceText)+": "+Integer.toString(product.getPrice()));
    }
        else{
            productName.setText(product.getName());
            productPrice.setText(ctx.getResources().getString(R.string.priceText)+": "+Integer.toString(product.getPrice()));
        }
        productImage.setImageResource(product.getImageID());

        if(product.isSelected())
            rowProduct.setBackgroundColor(parent.getResources().getColor(R.color.itemSelected));
        else
            rowProduct.setBackgroundColor(parent.getResources().getColor(R.color.itemUnselected));
        return rowProduct;
    }

}
